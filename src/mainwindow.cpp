#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "quizzclass.h"
#include <QPushButton>
#include <QTimer>
#include <QInputDialog>
#include <QDir>

void MainWindow::changeBack() {
    this->setStyleSheet("background-color: gray");

    this-> show();
}
void MainWindow::updateTime()
    {
        time1=time1.addSecs(-1);

        timerLabel ->setText(time1.toString("s") + "s");
        if(time1.toString("s") == "0" && round != 2)
        {
            this->setStyleSheet("background-color:red");
            this-> show();
            QTimer::singleShot(2000, this, SLOT(changeBack()));
            time1.setHMS(0,0,22);
            round++;
            m_button->setText(qc.getAnswer1(round));
            m_button2->setText(qc.getAnswer2(round));
            m_button3->setText(qc.getAnswer3(round));
            label1->setText(qc.getQuestion(round));
            QString pointsstr = QString::number(points);
            label2->setText(pointsstr);
        }
        else if(time1.toString("s") == "0" && round ==2)
        {
            this->setStyleSheet("background-color:red");
            this-> show();
            QTimer::singleShot(2000, this, SLOT(changeBack()));
            QString pointsstr = QString::number(points);
            round++;
            label2->setText(pointsstr);
            QString result = "Game over!" + this->label3->text() + " got " + this->label2->text()+ "/300 points. :)";
            resultText ->setText(result);
            resultText->setGeometry(QRect(500,100,500,200));
            resultText->setVisible(true);
            label4->setVisible(false);
            timer->stop();
            timerLabel ->setVisible(false);
            timerLabelText ->setVisible(false);
        }

    }


void MainWindow::handleButton1() {
    if(round < 3) {
        if(this->qc.getCorrectAnswer(round)==1) {
            points += 100;
            this->setStyleSheet("background-color:green");
            this-> show();
            time1.setHMS(0,0,22);
            QTimer::singleShot(2000, this, SLOT(changeBack()));

        } else {
            this->setStyleSheet("background-color:red");
            this-> show();
            time1.setHMS(0,0,22);
            QTimer::singleShot(2000, this, SLOT(changeBack()));
        }
        round++;
        if(round < 3){
            m_button->setText(qc.getAnswer1(round));
            m_button2->setText(qc.getAnswer2(round));
            m_button3->setText(qc.getAnswer3(round));
            label1->setText(qc.getQuestion(round));
            QString pointsstr = QString::number(points);
            label2->setText(pointsstr);
        } else if (round == 3) {
           QString pointsstr = QString::number(points);
            label2->setText(pointsstr);
            QString result = "Game over!" + this->label3->text() + " got " + this->label2->text()+ "/300 points. :)";
            resultText ->setText(result);
            resultText->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
            resultText->setGeometry(QRect(500,100,500,200));
            resultText->setVisible(true);
            label4->setVisible(false);
            timer->stop();
            timerLabel ->setVisible(false);
            timerLabelText ->setVisible(false);
        }

    }

}
void MainWindow::handleButton2(){
    if(round < 3) {
        if(this->qc.getCorrectAnswer(round)==2) {
            points += 100;
            this->setStyleSheet("background-color:green");
            this-> show();
            time1.setHMS(0,0,22);
            QTimer::singleShot(2000, this, SLOT(changeBack()));
        } else {
            this->setStyleSheet("background-color:red");
            this-> show();
            time1.setHMS(0,0,22);
            QTimer::singleShot(2000, this, SLOT(changeBack()));
        }
        round++;
        if(round < 3) {
            m_button->setText(qc.getAnswer1(round));
            m_button2->setText(qc.getAnswer2(round));
            m_button3->setText(qc.getAnswer3(round));
            label1->setText(qc.getQuestion(round));
            QString pointsstr = QString::number(points);
            label2->setText(pointsstr);
        } else if (round == 3) {
           QString pointsstr = QString::number(points);
            label2->setText(pointsstr);
            QString result = "Game over!" + this->label3->text() + " got " + this->label2->text()+ "/300 points. :)";
            resultText ->setText(result);
            resultText->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
            resultText->setGeometry(QRect(500,100,500,200));
            resultText->setVisible(true);
            label4->setVisible(false);
            timer->stop();
            timerLabel ->setVisible(false);
            timerLabelText ->setVisible(false);
        }
    }
}
void MainWindow::handleButton3(){
    if(round < 3) {
        if(this->qc.getCorrectAnswer(round)==3) {
            points += 100;
            this->setStyleSheet("background-color:green");
            this-> show();
            time1.setHMS(0,0,22);
            QTimer::singleShot(2000, this, SLOT(changeBack()));
        } else {
            this->setStyleSheet("background-color:red");
            this-> show();
            time1.setHMS(0,0,22);
            QTimer::singleShot(2000, this, SLOT(changeBack()));
        }
        round++;
        if(round < 3) {
            m_button->setText(qc.getAnswer1(round));
            m_button2->setText(qc.getAnswer2(round));
            m_button3->setText(qc.getAnswer3(round));
            label1->setText(qc.getQuestion(round));
            QString pointsstr = QString::number(points);
            label2->setText(pointsstr);
        } else if (round == 3) {
            QString pointsstr = QString::number(points);
            label2->setText(pointsstr);
            QString result = "Game over!" + this->label3->text() + " got " + this->label2->text()+ "/300 points. :)";
            resultText ->setText(result);
            resultText->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
            resultText->setGeometry(QRect(500,100,500,200));
            resultText->setVisible(true);
            label4->setVisible(false);
            timer->stop();
            timerLabel ->setVisible(false);
            timerLabelText ->setVisible(false);
        }
    }
}



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->round = 0;
    this->points = 0;
    QuizzClass qc  = QuizzClass();

    //Background is a picture
    this->setStyleSheet("background-color: gray");


    //Laber which contains "Timer" text
    this->timerLabelText = new QLabel(this);
    timerLabelText->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
    timerLabelText->setWordWrap(true);
    timerLabelText->setText("Timer");
    timerLabelText->setGeometry(QRect(10,70,40,40));
    timerLabelText ->setVisible(true);
    timerLabelText -> setStyleSheet("QLabel {color: darkblue; background-color: white;} QLabel:hover {color: darkred;}");
    //Label which contains timer
    this->timerLabel = new QLabel(this);
    timerLabel->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
    timerLabel->setGeometry(QRect(10,100,40,40));
    timerLabel ->setVisible(true);
    time1.setHMS(0,0,20);
    connect(timer,SIGNAL(timeout()), this, SLOT(updateTime()));
    timerLabel -> setStyleSheet("QLabel { background-color : white; color : black; border: 1px solid black; border-width: 2px;  border-color: darkgreen;} QLabel:hover {color: darkred;}");
    QPropertyAnimation animationTimer(timerLabel, "geometry");
    animationTimer.setDuration(1000);
    animationTimer.setStartValue(QRect(10,100,40,40));
    animationTimer.setEndValue(QRect(10,110,40,40));

    animationTimer.start();


    this->m_button = new QPushButton(qc.getAnswer1(this->round), this);
    // set size and location of the button
    this->m_button->setGeometry(QRect(QPoint(100, 400), QSize(200, 50)));
    connect(m_button, SIGNAL(clicked()), this, SLOT(handleButton1()));
    m_button ->setStyleSheet("QPushButton {color: black; background-color: white; border: 1px solid black; border-radius: 10px} QPushButton:hover {color: orange;}");
    this->m_button2 = new QPushButton(qc.getAnswer2(this->round), this);
    // set size and location of the button
    this->m_button2->setGeometry(QRect(QPoint(300, 400), QSize(200, 50)));
    connect(m_button2, SIGNAL(clicked()), this, SLOT(handleButton2()));
    m_button2 ->setStyleSheet("QPushButton {color: black; background-color: white; border: 1px solid black; border-radius: 10px} QPushButton:hover {color: orange;}");
    this->m_button3 = new QPushButton(qc.getAnswer3(this->round), this);
    // set size and location of the button
    this->m_button3->setGeometry(QRect(QPoint(500, 400), QSize(200, 50)));
    connect(m_button3, SIGNAL(clicked()), this, SLOT(handleButton3()));
    m_button3 ->setStyleSheet("QPushButton {color: black; background-color: white; border: 1px solid black; border-radius: 10px} QPushButton:hover {color: orange;}");
    this->label1 = new QLabel(this);
    label1->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
    label1->setWordWrap(true);
    label1->setText(qc.getQuestion(round));
    label1->setGeometry(QRect(10,200,300,50));
    label1 ->setVisible(true);
    label1 ->setStyleSheet("QLabel { background-color : white; color : black; border: 1px solid black; border-radius: 10px}");
    this->label2 = new QLabel(this);
    label2->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
    label2->setWordWrap(true);
    QString pointsstr = QString::number(points);
    label2->setText(pointsstr);
    label2->setGeometry(QRect(10,0,70,70));
    label2 ->setVisible(true);
    label2 ->setStyleSheet("QLabel { background-color : white; color : black; border: 1px solid black; border-radius: 10px}");
    resultText = new QLabel(this);
    resultText->setGeometry(QRect(300,0,500,100));
    resultText->setVisible(false);
    bool ok;
    this->label3 = new QLabel(this);
    QString text = QInputDialog::getText(this, tr("Gamer name."),
                                         tr("Name:"), QLineEdit::Normal,
                                         QDir::home().dirName(), &ok);
    if (ok && !text.isEmpty()) {
        label3->setText(text);
        //qc.setName(text);
        timer ->start(1000);
    }
    label3->setWordWrap(true);
    label3->setGeometry(QRect(600,0,200,70));
    label3 ->setVisible(true);
    label3 ->setStyleSheet("QLabel { background-color : white; color : black; border: 1px solid black; border-radius: 10px}");

    label4 = new QLabel(this);
    QPixmap pix(":/images/globe.png");
    QPixmap scaled = pix.scaled(200,200,Qt::KeepAspectRatio,Qt::SmoothTransformation);
    label4->setPixmap(scaled);
    QBitmap map(200,200);
    map.fill(Qt::color0);
    QPainter painter( &map );
    painter.setBrush(Qt::color1);
    painter.drawRoundedRect( 0, 0, 200, 200,15,15);
    scaled.setMask(map);
    label4->setPixmap(scaled);
    label4->setGeometry(QRect(300,0,200,200));
    label4 ->setVisible(true);





}

MainWindow::~MainWindow()
{
    delete ui;
}



