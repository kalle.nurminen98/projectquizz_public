#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QLabel>
#include <QTime>
#include <QTimer>
#include <QPainter>
#include <QPropertyAnimation>
#include "quizzclass.h"
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    Ui::MainWindow *ui;
    QPalette pal;
    QuizzClass qc;
    QPushButton *m_button;
    QPushButton *m_button2;
    QPushButton *m_button3;
    QLabel *label1;
    QLabel *label2;
    QLabel *label3;
    QLabel *label4;
    QLabel *resultText;
    QLabel *timerLabel;
    QLabel *timerLabelText;
    int round;
    int points;
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
public slots:
    void updateTime();
    void changeBack();
    void handleButton1();
    void handleButton2();
    void handleButton3();
public:
    QTime *time = new QTime;
    QTime time1;
    QTimer *timer = new QTimer;

};
#endif // MAINWINDOW_H

