#ifndef QUIZZCLASS_H
#define QUIZZCLASS_H
#include <QString>
#include <QVector>

class QuizzClass
{
public:
    QuizzClass();
    QString getName();
    int getPoints();
   // void SetAnswersAndQuestions();
    QString getQuestion(int nro);
    QString getAnswer1(int nro);
    QString getAnswer2(int nro);
    QString getAnswer3(int nro);
    int getCorrectAnswer(int nro);
    void setRound(int r);


private:
    int points;
    int round;
    QString nickname;
    QVector<QString> question;
    QVector<QString> answer1;
    QVector<QString> answer2;
    QVector<QString> answer3;
    QVector<int> correctAnswer;

};

#endif // QUIZZCLASS_H
