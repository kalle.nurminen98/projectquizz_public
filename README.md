# ProjectQuizz

Idea was to create quizgame and add there some features like visualation.

In this project Ilona's tuesday project was used as a basis. It contained quizzclass.cpp and .h and mainwindow done with pushbuttons and pointslabel and also it contained three rounds.

# Usage

Take a git clone on project and take it to your computer.

# About the project

This was made by Kalle Nurminen and Ilona Skarp for Noroff and Experis Academy Finland.

# Responsibilities in this project

Kalle: Made timer to work in this project and modified hover color settings with some labels and modified picture to work with Resources.qrc.

Ilona: Used SingleShot Timer to control colors, made Button slots, Picture's rouded corners, username prompt. 

# Maintainers

[Ilona Skarp] (https://gitlab.com/iskarp)

[Kalle Nurminen] (https://gitlab.com/kalle.nurminen98)
